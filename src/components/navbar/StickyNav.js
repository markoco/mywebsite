import React, {useRef,useState} from 'react';
import {TweenMax} from 'gsap';
import Navbar from './Navbar';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBars } from '@fortawesome/free-solid-svg-icons';
const StickyNav = () => {
    const[showNav,setShowNav] = useState(false);

    let nav = useRef(null);
    let overlay = useRef(null);
    let mobilenav = useRef(null);

    window.onscroll = function() {
        if ((window.scrollY) >= (window.innerHeight)) {
            TweenMax.to(
                nav,
                .4,{
                    opacity: 1,
                    height: "60px",
                    display: "block",
                    position: "sticky"
                },
            )
            TweenMax.to(
                overlay,
                .4,{
                    opacity: 1,
                    height: "60px"
                },
            )
        }else{
            TweenMax.to(
                nav,
                .4,{
                    opacity: 0,
                    height: "0px",
                    display: "none"
                },
            )
            TweenMax.to(
                overlay,
                .4,{
                    opacity: 0,
                    height: "0px"
                },
            )
        }
    };

    const mobileNavAnimation = () =>{
        setShowNav(!showNav);
        if(!showNav){
            TweenMax.to(
                mobilenav,
                .4,{
                    opacity: 1,
                    width: "100%"
                },
            )
        }else{
            TweenMax.to(
                mobilenav,
                .4,{
                    opacity: 0,
                    width: "0%"
                },
            ) 
        }
    }


    return ( 
        <>
        
        <div   
            className="nav" 
            ref={el => {nav = el}}
            style={{
                height:"0px",
                width:"100%",
                display: "none",
                top: 0, 
                backgroundImage: "url(images/bg1.jpg)",
                backgroundPosition: "center",
                objectFit: "cover",
                zIndex: 4
            }}
        >
            <div 
                ref={el => {overlay = el}}
                style={{
                 width:"100%",
                 height:"0px",
                 backgroundColor:"rgba(31, 58, 147, .7)",
                 display:"flex",
                 alignItems:"center",
                 justifyContent:"space-between"

            }}> 
                <h2 className="text-light px-4">Mark Kristian Oco</h2>
                <div>
                    <Navbar/>
                </div>
                <span 
                    onClick={mobileNavAnimation} 
                    className="hamburger"
                >
                    <i style={{fontSize:"45px", color:"white"}}> 
                        <FontAwesomeIcon icon={faBars} />
                    </i>
                </span>
            </div>
        </div>
        <div  
            ref={el => {mobilenav = el}} 
            className="mobileNav" 
        >
            <div 
                style={{width:"70%", height:"100vh", 
                float:"left", 
                backgroundImage: "url(images/bg1.jpg)",
                backgroundSize:"cover",
                backgroundPosition:"center"}}
            >
                <div 
                    style={{
                        width:"100%",
                        height:"100%",
                        backgroundColor:"rgba(31, 58, 147, .7)"}}
                >
                    <h1 className="text-light text-left p-5">Mark Kristian Oco</h1>
                    <Navbar/>
                    
                </div>
            </div>
            <div 
                onClick={mobileNavAnimation} 
                style={{
                    width:"30%",
                    height:"100vh",
                    float:"right"}}
            >
                
            </div>
        </div>
        </>
    );
}
 
export default StickyNav;