import React from 'react';

const Project = props => {
    return ( 
        <div>
					<div className="projectContainer d-flex align-items-center justify-content-center flex-wrap m-3">
						<img alt={props.img} src={props.img}/>
						<div className="p-5">
							<h3>{props.title}</h3>
							<p>{props.description}</p>
							{props.stacks.map((stack,index)=>(
								<span key={index} className="badge badge-dark m-1">{stack}</span>
							))}
							<div className="my-3 d-flex">
								<div style={{
										backgroundImage: "url(images/bg1.jpg)",
										backgroundPosition: "center top",
										textDecoration: "none",
										width: "100px",
									}}
								>
									<a
									href={props.demo} 
									target="_blank"
									rel="noopener noreferrer"
									className="text-light px-auto py-2 text-center"
									style={{
										backgroundColor:"rgba(31, 58, 147, .7)",
										textDecoration: "none",
										width: "100%",
										height: "100%",
										display: "block"
									}}
									>
										View Demo
									</a>
								</div>
		
								<div style={{
										backgroundImage: "url(images/bg1.jpg)",
										backgroundPosition: "center top",
										textDecoration: "none",
										width: "100px",
										marginLeft: "5px"
									}}
								>
									<a
										href={props.source} 
										target="_blank"
										rel="noopener noreferrer"
										className="text-light px-auto py-2 text-center"
										style={{
											backgroundColor:"rgba(31, 58, 147, .7)",
											textDecoration: "none",
											width: "100%",
											height: "100%",
											display: "block"
										}}
									>
										View Source
									</a>
								</div>
							</div>
						</div>
					</div>
					<hr></hr>
	
					
        </div>
    );
}
 
export default Project;