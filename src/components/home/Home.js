import React from 'react';
import Navbar from '../navbar/Navbar'
const Home = () => {

    return ( 
        <div 
            id ="Home"
            className="container-fluid vh-100 text-center p-0 position-relative">
                <img 
                    src="images/bg1.jpg"
                    alt="backgroundImg"
                    style={{
                        width:"100%",
                        height:"100%"
                        }} 
                />
                <div 
                    style={{
                        width:"100%",
                        height:"100%",
                        position:"absolute",
                        top: 0,
                        backgroundColor:"rgba(31, 58, 147, .7)",
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        justifyContent: "center",
                        color: "white"
                    }}
                >
                    <h1 style={{fontSize:"4rem", textAlign:"center"}}>Mark Kristian Oco</h1>
                    <Navbar />
                    <div style={{width:"50%"}}>
                        I am a highly driven and creative web developer with knowledge in designing and developing robust websites. Please scroll down to see what I can do for you.
                    </div>
                </div>
        </div>
    );
}
 
export default Home;

