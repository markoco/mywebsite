import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faDownload } from '@fortawesome/free-solid-svg-icons';
const NavLink = ({text,handleShowForm,link}) => {
    const[selected,setSelected] = useState("Home")
    return ( 
        <li 
            style={selected === text
            ? 
                {
                    cursor:"pointer",
                    backgroundColor: "rgba(225, 225, 225, 0.4)"
                }
            :
                {
                    cursor:"pointer"
                }
            }
        >
            {text === 'Contact'
                ?<span
                    onClick={handleShowForm}
                    style={{
                        textDecoration:"none", 
                        color:"white", 
                        fontSize:"18px", 
                        textAlign:"center"
                }}>
                    {text}
                </span>
                : text === 'Resume' 
                ?<a 
                    href={link}
                    target="_blank"
                    rel="noopener noreferrer"
                    style={{
                        textDecoration:"none", 
                        color:"white", 
                        fontSize:"18px", 
                        textAlign:"center"
                    }}
                >
                    <i className="mr-2" style={{fontSize:"15px", color:"white"}}> 
                        <FontAwesomeIcon icon={faDownload} />
                    </i>
                    {text}
                </a>
                :<a 
                    href={"#"+text}
                    style={{
                        textDecoration:"none", 
                        color:"white", 
                        fontSize:"18px", 
                        textAlign:"center"
                }}>
                    {text}
                </a>
            }
        </li>
    );
}
 
export default NavLink;