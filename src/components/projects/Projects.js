import React from 'react';
import Project from './Project';
const Projects = () => {
    return ( 
        <div id="Projects" className="pt-5">
            <h1 className="p-3 text-center">View My Projects</h1>
            <hr></hr>
            <div className="d-flex flex-wrap justify-content-center col-lg-12">
                <Project
                    title = "Calaguas Hotel Booking System"
                    description ="Hotel Booking System with admin interaction. Users can search and book available rooms in specific dates. Users can pay via stripe using their credit cards. Admin can monitor the sales between two selected dates."
                    stacks={["ReactJS","MongoDb","ExpressJS","NodeJs","Bootstrap","Javascript","ChartJS"]}
                    img="images/hotel.png"
                    demo="https://cocky-mcclintock-06fde1.netlify.app/"
                    source="https://gitlab.com/markoco/hotel.git"
                />
                <Project
                    title = "Civil Service Quiz Game"
                    description ="A website for those who are aspiring to pass the Civil Service Exam. Users can take a quiz, post delete and update their own quiz, report a quiz, unlock achievements and more. "
                    stacks={["Laravel","PHP","MySQL","HTML","CSS","Bootstrap","Javascript"]}
                    img="images/CSC2.png"
                    demo="http://blooming-hollows-24907.herokuapp.com/"
                    source="https://gitlab.com/markoco/laravel-project-2.git"
                />
                <Project
                    title = "COVID-19 Tracker"
                    description ="A COVID-19 tracker which can be used to easily track the status of the pandemic disease such as the total number of infected people and total number of death. Users can also search data of a specific country."
                    stacks={["react.js","javascript","bootstrap"]}
                    img="images/covid19tracker.png"
                    demo="http://pacific-hollows-56336.herokuapp.com/"
                    source="https://gitlab.com/markoco/covid.git"
                />
                <Project
                    title = "E-Commerce Website"
                    description ="Shopping website with admin interaction. Users can shop various goods/items. It has a filter by category and item. Admin can cancel a transaction. He can also demote and promote users."
                    stacks={["Laravel","PHP","MySQL","HTML","CSS","Bootstrap","Javascript"]}
                    img="images/shoppinglaravel.png"
                    demo="http://guarded-cliffs-63701.herokuapp.com/"
                    source="https://gitlab.com/markoco/laravel-project-1.git"
                />
                <Project
                    title = "SweetSell Cake Shop Web Design"
                    description ="Mobile first approached. It is a responsive web design for my friend's cakeshop. It is developed using HTML, CSS and Javascript. It is only an informative website, it doesn't have a backend connection."
                    stacks={["HTML","CSS","Javascript"]}
                    img="images/cakeshop.png"
                    demo="https://markoco.gitlab.io/capstone1/#homepage"
                    source="https://gitlab.com/markoco/capstone1.git"
                />  
            </div>
           
        </div>
        
     );
}
 
export default Projects;