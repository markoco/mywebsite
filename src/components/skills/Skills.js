import React from 'react';

const Skills = () => {
    return ( 
        <div id="Skills" className="p-5">
            <h1 className="p-3 text-center">My Skills</h1>
            <hr></hr>
            <div className="row d-flex align-items-center justify-content-center mx-0 px-0">
                <div className="col-lg-6 text-center">
                    <h3 className="text-center">Web Development</h3>
                    <img alt="html" title="HTML" className="m-3" src="/images/html.png" height="45px"/>
                    <img alt="css" title="CSS" className="m-3" src="/images/css.png" height="45px"/>
                    <img alt="javascript" title="Javascript" className="m-3" src="/images/javascript.png" height="45px"/>
                    <img alt="Bootstrap" title="Bootstrap" className="m-3" src="/images/bootstrap.png" height="45px"/>
                    <img alt="PHP" title="PHP" className="m-3" src="/images/php.png" height="40px"/>
                    <img alt="MySQL" title="MySQL" className="m-3" src="/images/mysql.webp" height="45px"/>
                    <img alt="Laravel" title="Laravel" className="m-3" src="/images/laravel.png" height="45px"/>
                    <img alt="MongoDbL" title="MongoDb" className="m-3" src="/images/mongodb.png" height="45px"/>
                    <img alt="ExpressJs" title="ExpressJs" className="m-3" src="/images/express.png" height="45px"/>
                    <img alt="ReactJs" title="ReactJs" className="m-3" src="/images/reactjs.webp" height="45px"/>
                    <img alt="NodeJs" title="NodeJs" className="m-3" src="/images/node.png" height="45px"/>
                    <img alt="Postman" title="Postman" className="m-3" src="/images/postman.png" height="45px"/>
                    <img alt="Gitlab" title="Gitlab" className="m-3" src="/images/gitlab.webp" height="45px"/>
                    <img alt="Linux" title="Linux" className="m-3" src="/images/linux.png" height="40px"/>
                </div>
            </div>
            <div className="row d-flex align-items-center justify-content-center mx-0 px-0 my-5">
                <div className="col-lg-6 text-center">
                    <h3 className="text-center">Data Reporting</h3>
                    <img alt="Google Docs" title="Google Docs" className="m-3" src="/images/docs.png" height="45px"/>
                    <img alt="Google Spreadsheets" title="Google Spreadsheets" className="m-3" src="/images/sheets.webp" height="45px"/>
                    <img alt="Google Slides" title="Google Slides" className="m-3" src="/images/slide.png" height="45px"/>
                    <img alt="Data Studio" title="Data Studio" className="m-3" src="/images/datastudio.png" height="45px"/>
                    <img alt="Google Apps Script" title="Google Apps Script" className="m-3" src="/images/gas.png" height="45px"/>
                </div>
            </div>
        </div>
     );
}
 
export default Skills;