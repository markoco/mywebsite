import React from 'react';

const Footer = () => {
    return (  
        <div style={{
            width:"100%", 
            height:"50px",
            backgroundImage: "url(images/bg1.jpg)",
            position: "relative"
        }}>
            <div style={{
                width:"100%",
                height:"50px",
                position:"absolute",
                top: 0,
                backgroundColor:"rgba(31, 58, 147, .7)",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                justifyContent: "center",
            }}
            >
                <p className="text-light text-center mt-3">Developed by Mark Kristian Oco 2020</p>
            </div>

        </div>
    );
}
 
export default Footer;