import React, { useState } from 'react';
import NavLink from './NavLink';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

const Navbar = () => {
    const [showForm,setShowForm] = useState(false);

    const handleShowForm = () =>{
        setShowForm(true)
    }

    return ( 
        <>
        <nav style={{padding:"30px"}}>
            <ul style={{listStyleType:"none"}}>
                <NavLink 
                    text = "Home"
                />
                <NavLink 
                    text = "Projects"
                />
                <NavLink 
                    text = "Skills"
                />
                <NavLink 
                    text = "Contact"
                    handleShowForm = {handleShowForm}
                />
                <NavLink 
                    text = "Resume"
                    link = 'https://www.canva.com/design/DAD4DBgVYs0/Uc0E3yOk_hYWUP1SntOrSQ/view?utm_content=DAD4DBgVYs0&utm_campaign=designshare&utm_medium=link&utm_source=sharebutton'
                />
            </ul>
        </nav>
        <Modal isOpen={showForm} toggle={()=>setShowForm(false)}>
            <ModalHeader  
                style={{
                    width:"100%",
                    position: "relative",
                    backgroundImage: "url(images/bg1.jpg)"
                    }} 
                className="position-relative" 
                toggle={()=>setShowForm(false)}
            >
                <div style={{
                    position: "absolute",
                    top: 0,
                    left: 0,
                    backgroundColor:"rgba(31, 58, 147, .7)",
                    width:"100%",
                    height:"100%"
                }}>
                    <h4 className="text-light p-2">Contact Me</h4>
                </div>
            </ModalHeader>
            <ModalBody className="pt-5 d-flex flex-column align-items-center justify-content-center">
                <p>Phone Number: 09287666301</p>
                <p>Address: Makati City, but willing to relocate.</p>
                <p>E-mail: markv.oco@gmail.com</p>
                <div>
                    <a href="https://www.linkedin.com/in/mark-kristian-oco-a784a4110/" target="_blank" rel="noopener noreferrer">
                        <img alt="linkedin" title="LinkedIn" className="mx-1" src="images/linkedin.webp" width="30px" />
                    </a>
                    <a href="https://www.facebook.com/mark.v.oco" target="_blank" rel="noopener noreferrer">
                        <img alt="facebook" title="Facebook" className="mx-1" src="images/facebook.webp" width="30px" />
                    </a>
                    <a href="https://gitlab.com/markoco" target="_blank" rel="noopener noreferrer">
                        <img alt="gitlab" title="GitLab" className="mx-1" src="images/gitlab.png" width="30px" />
                    </a>
                </div>
            </ModalBody>
        </Modal>
        </>
    );
}
 
export default Navbar;