import React from 'react';
import Home from './components/home/Home';
import Projects from './components/projects/Projects';
import Footer from './components/Footer';
import StickyNav from './components/navbar/StickyNav';
import Skills from './components/skills/Skills';
const App = () => {
  return (
    <div>
      <Home />
      <StickyNav/>
      <Projects />
      <Skills/>
      <Footer />
    </div>
  );
}

export default App;
